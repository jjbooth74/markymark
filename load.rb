require "bundler/setup"
require "mysql2"

db = Mysql2::Client.new(host: "localhost", username: "root", password: "admin", database: "markymark")
db.query("delete from words;")

module Markymark
  class Token
    attr_accessor :word, :next_word
    def initialize(word, next_word)
      @word = word
      @next_word = next_word
    end
  end

  class Util
    def self.tokenize_line(line="")
      line.split(" ").reduce({}) do |acc, token|
        word = acc[:prev_word] || "NEWLINE"
        new_acc = {}
        acc[:token_list] ||= []
        new_acc[:token_list] = acc[:token_list]
        new_acc[:token_list] << Token.new(word, token.downcase)
        new_acc[:prev_word] = token.downcase
        new_acc
      end
    end
  end
end

token_count = 0
File.open("./source") do |file|
  file.each do |line|
    tokens = Markymark::Util.tokenize_line(line)
    tokens[:token_list].each do |t|
      db.query("INSERT INTO words (word, next) VALUES ('#{db.escape(t.word)}', '#{db.escape(t.next_word)}')")
    end
    token_count = token_count + tokens.count
  end
end

puts "Inserted #{token_count} tokens"
