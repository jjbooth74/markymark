require "bundler/setup"
require "mysql2"

db = Mysql2::Client.new(host: "localhost", username: "root", password: "admin", database: "markymark")

word = "NEWLINE"

module Markymark
  class Util
    def self.options(db, word)
      results = db.query <<-eos
        select w.word, w.next, count(w.next) as 'count' from words w where w.word = '#{db.escape(word)}' 
group by w.word, w.next;
      eos
      cumulative_sum = 0
      results.map do |r|
        r["cumulative_sum"] = cumulative_sum + r["count"]
        cumulative_sum = cumulative_sum + r["count"]
        r
      end
    end

    # assumes options is ordered by cumulative_sum
    def self.pick(options)
      total = options.last["cumulative_sum"]
      r = Random.rand(1..total)
      # TODO: Break this out so i can test without depending on random
      options.take_while { |o| r >= o["cumulative_sum"] }.last
    end
  end
end

# TODO: Make this not gross.
song = ""
(1..4).each do |i| 
  (1..8).each do |j|
    options = Markymark::Util.options(db, word)
    pick = options.nil? || options.empty? ? nil : Markymark::Util.pick(options)
    word = pick.nil? ? "NEWLINE" : pick["next"]

    song << " "
    song << word.gsub("NEWLINE", "\n")
  end
  song << "\n"
end

puts song