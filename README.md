# wat
This is a song generator that uses markov chains to create a song.
You can feed it any corpus of work but it works best if you have lots of songs of a similar style (think: the entire beatles anthology, the top 100 country songs of past year, etc)
Right now it's pretty hacky and everything is hardcoded just because I wanted to play with markov models - there's a lot that could be done to improve the codebase as well as the algorithm.

# Setup
1. Create a source file of song lyrics called `source` or use the existing one which has a bunch of CCM songs in it.
2. The `source` file is a little picky - it can't have blank lines, and you probably want to strip out any VERSE or CHORUS tags.
1. Create a DB in a local mysql instance named `markymark`
1. `CREATE TABLE words (   id BIGINT NOT NULL AUTO_INCREMENT,   word VARCHAR(100) NOT NULL,   next VARCHAR(100),   PRIMARY KEY(id) );` 
2. `bundle exec ruby load.rb`

# To run
1. `bundle exec ruby generate.rb`


# To improve

2. Better handle line breaks (e.g. which words end lines)
1. Add rhyme schemes
3. Make things configurable via env or CLI parameters
